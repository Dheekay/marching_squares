﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class VoxelGrid : MonoBehaviour
{
	public GameObject m_VoxelPrefab;

	private int m_Resolution = 8;
	private bool[] m_Voxels;
	private float m_VoxelSize;
	private Material[] m_VoxelMaterials;

	public void Initialize(int resolution, float size)
	{
		this.m_Resolution = resolution;
		m_VoxelSize = size / m_Resolution;
		m_Voxels = new bool[m_Resolution * m_Resolution];
		m_VoxelMaterials = new Material[m_Voxels.Length];

		for(int i = 0, y = 0; y < m_Resolution; y++)
		{
			for(int x = 0; x < m_Resolution; x++, i++)
			{
				CreateVoxel(i, x, y);
			}
		}

		SetVoxelColors();
	}

	private void CreateVoxel(int i, int x, int y)
	{
		GameObject o = Instantiate(m_VoxelPrefab, transform) as GameObject;
		o.transform.localPosition = new Vector3((x + 0.5f) * m_VoxelSize, (y + 0.5f) * m_VoxelSize);
		o.transform.localScale = Vector3.one * m_VoxelSize * 0.9f;

		m_VoxelMaterials[i] = o.GetComponent<MeshRenderer>().material;
	}

	private void SetVoxelColors()
	{
		for(int i = 0; i < m_Voxels.Length; i++)
		{
			m_VoxelMaterials[i].color = m_Voxels[i] ? Color.black : Color.white;
		}
	}

	public void SetVoxel(int x, int Y, bool state)
	{
		m_Voxels[Y * m_Resolution + x] = state;
		SetVoxelColors();
	}
}
