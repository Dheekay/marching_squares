﻿using UnityEngine;
using System.Collections;
using System;

public class VoxelMap : MonoBehaviour
{
	public float m_Size = 2f;

	public int m_VoxelResolution = 8;
	public int m_ChunkResolution = 2;

	public VoxelGrid m_VoxelGridPrefab;

	private VoxelGrid[] m_Chunks;
	private float m_ChunkSize, m_VoxelSize, m_HalfSize;

	private void Awake()
	{
		m_HalfSize = m_Size * 0.5f;
		m_ChunkSize = m_Size / m_ChunkResolution;
		m_VoxelSize = m_ChunkSize / m_VoxelResolution;

		m_Chunks = new VoxelGrid[m_ChunkResolution * m_ChunkResolution];

		for (int i = 0, y = 0; y < m_ChunkResolution; y++)
		{
			for(int x = 0; x < m_ChunkResolution; x++, i++)
			{
				CreateChunk(i, x, y);
			}
		}

		BoxCollider box = gameObject.AddComponent<BoxCollider>();
		box.size = new Vector3(m_Size, m_Size);
	}

	private void Update()
	{
		if (Input.GetMouseButton(0))
		{
			RaycastHit hitInfo;
			if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
			{
				if(hitInfo.collider.gameObject == gameObject)
				{
					EditVoxels(transform.InverseTransformPoint(hitInfo.point));
				}
			}
		}
	}

	private void EditVoxels(Vector3 point)
	{
		int voxelX = (int)((point.x + m_HalfSize) / m_VoxelSize);
		int voxelY = (int)((point.y + m_HalfSize) / m_VoxelSize);
		int chunkX = voxelX / m_VoxelResolution;
		int chunkY = voxelY / m_VoxelResolution;

		voxelX -= chunkX * m_VoxelResolution;
		voxelY -= chunkY * m_VoxelResolution;
		m_Chunks[chunkY * m_ChunkResolution + chunkX].SetVoxel(voxelX, voxelY, true);
	}

	private void CreateChunk(int i, int x, int y)
	{
		VoxelGrid chunk = Instantiate(m_VoxelGridPrefab, transform) as VoxelGrid;
		chunk.Initialize(m_VoxelResolution, m_ChunkSize);
		chunk.transform.localPosition = new Vector3(x * m_ChunkSize - m_HalfSize, y * m_ChunkSize - m_HalfSize);
		m_Chunks[i] = chunk;
	}
}
